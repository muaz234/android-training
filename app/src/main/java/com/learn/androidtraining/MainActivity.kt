package com.learn.androidtraining

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val button : Button = this.findViewById(R.id.submit_btn)
        val hello_world : TextView = findViewById(R.id.hello_world)
        val email: EditText = findViewById(R.id.email_field)
        var number = 5
        button.setOnClickListener {
            Toast.makeText(this, "Button pressed "+number, Toast.LENGTH_LONG).show()
            number +=  1
            hello_world.setText("Somebody change my value.")
            hello_world.text = email.text.toString()
        }
    }
}
